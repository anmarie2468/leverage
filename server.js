const express = require("express");
const app = express();
const path = require("path");
const port = 3000;
const userDetails = require("./userDetails.json");
const viewRoutes = require("./routes/viewRoutes.js");

const aLoggerMiddleware = (req, res, next) => {
    console.log(req.url, res.method, res.statusCode);
    next();
};

const serveLandingPage = (req, res) => {
    res.sendFile(path.join(__dirname, "public/index.html"));
};

app.use(aLoggerMiddleware);
app.use(express.static("public"));
app.use(viewRoutes);

//app.get("/", function (req, res) {
//    res.send("Hello TechWise!");
//});

app.get("/portfolio", (req, res) => {
    res.sendFile(path.join(__dirname + "./public/portfolio.html"));
});

app.get("/", serveLandingPage);

app.get("/about", serveLandingPage);

app.get("/api/userBalance", (req, res) => {
    console.log(userDetails);
    res.json(userDetails.info);
});


app.listen(port, () => {
    console.log('Example app listening on port ${port}');
});