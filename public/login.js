const MDCTextField = mdc.textField.MDCTextField;
const mdcTextFields = [].map.call(
  document.querySelectorAll(".mdc-text-field"),
  function (el) {
    return new MDCTextField(el);
  }
);

console.log(document.forms);
const loginForm = document.forms[0];
const registrationForm = document.forms[1];
console.log(registrationForm);
const registerButton = registrationForm[6];

function validateEligibility(event) {
  event.preventDefault();
  if (noEmptyFields() && passwordsMatch() && legalCheckboxChecked()) {
    console.log("so valid");
  } else {
    console.log("not so valid");
  }
}
registerButton.addEventListener("click", validateEligibility);

function isNotEmpty(input) {
  return input.value !== "";
}

const usernameInput = registrationForm[0];
const passwordInput = registrationForm[1];
const confirmPasswordInput = registrationForm[2];
const securityQuestionInput = registrationForm[3];
const securityAnswerInput = registrationForm[4];

const registrationFormInputs = [
  usernameInput,
  passwordInput,
  confirmPasswordInput,
  securityQuestionInput,
  securityAnswerInput,
];

function noEmptyFields() {
  return registrationFormInputs.every(isNotEmpty);
}

function passwordsMatch() {
  return passwordInput.value === confirmPasswordInput.value;
}

function legalCheckboxChecked() {
  return registrationForm[5].checked;
}

