const router = require('express').Router();
const path = require("path");

const serveLandingPage = (req, res) => {
    res.sendFile(path.join(__dirname, "public/index.html"));
};

app.get("/", serveLandingPage);

app.get("/about", serveLandingPage);

app.get("/portfolio", (req, res) => {
    res.sendFile(path.join(__dirname + "./public/portfolio.html"));
});

module.export = router;
