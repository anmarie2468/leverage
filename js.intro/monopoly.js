function Player(name, location, money = 500) {
    this.name = name;
    this.location = location;
    this.money = money;
}

const player1 = new Player("Player One", 0);
const player2 = new Player("Player Two", 0);

console.log(player1);

const board = [];

function Property(name, price, rent, houseRent, hotelRent) {
    this.name = name;
    this.price = price;
    this.rent = rent;
    this.houseRent = houseRent;
    this.hotelRent = hotelRent;
    this.owner = null;
}

const properties = [
    ["Go!", 0, 0, 0, 0],
    ["Mediterranian Ave", 60, 4, 20, 100],
    ["Baltic Ave", 60, 5, 25, 120],
    ["Free Parking", 0, 0, 0, 0],
    ["Park Place", 350, 60, 150, 1000],
    ["Board Walk", 400, 80, 200, 1200],
];
console.log(properties);

for(const property of properties) {
    board.push(new Property(...property));
}

console.log(board);

// step 2:
function rollDice(sides = 6) {
    return 1 + Math.floor(Math.random() * sides)
}

function rollDice() {
    return rollDice(3) + rollDice(3);
}

for (let i = 0; i < 10; i++) {
    console.log(rollDice())
}